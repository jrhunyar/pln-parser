import xml.etree.ElementTree as ET
import tkinter as tk
from tkinter import filedialog, Text
import os
from os import path

window = tk.Tk()

header = tk.Label(window, text='MSFS PLN Editor', fg='#003333', font=('Helvetica', 16, 'bold'), pady=5)
header.pack()

def openPLN():
    for widget in frame.winfo_children():
        widget.destroy()

    dir = path.expandvars(r'%APPDATA%\Microsoft Flight Simulator')
    filename = filedialog.askopenfilename(initialdir=dir, title="Select File",
                                          filetypes=([('PLN Files','*.PLN')]))

    parsePLN(filename)

myWaypoints = []

def parsePLN(filename):
    # father forgive me for I have sinned 3x
    global tree
    tree = ET.parse(filename)

    global root
    root = tree.getroot()

    tree.write('init.PLN', encoding="UTF-8", xml_declaration=True)
    
    global waypoints
    waypoints = root.findall('./FlightPlan.FlightPlan/ATCWaypoint')

    l = len(waypoints)

    currow = 0
    curcol = 0

    def qf(quickPrint):
        print(quickPrint)

    for i, wp in enumerate(waypoints):
        ID = wp.get('id')
        ATCWaypointType = wp.find('ATCWaypointType').text
        WorldPosition = wp.find('WorldPosition').text

        if ATCWaypointType == 'Airport':
            ICAO = wp.find('ICAO/ICAOIdent').text
        else:
            ICAO = ID

        WPLabel = tk.Button(frame, text=ICAO, padx=9, pady=20, width=14,
                             relief=tk.RIDGE, fg='white', bg='#333333',
                             command=lambda: qf(ICAO))

        myWaypoint = {
            'ID': ID,
            'ICAO': ICAO,
            'Type': ATCWaypointType,
            'Coords': WorldPosition,
            'label': WPLabel
        }

        myWaypoints.append(myWaypoint)

        def on_enter(e):
            WPLabel['background'] = 'green'

        def on_leave(e):
            WPLabel['background'] = '#333333'

        WPLabel.bind("<Enter>", on_enter)
        WPLabel.bind("<Leave>", on_leave)

        if i == 0 or i == 6 or i == 12 or i == 18:
            curcol = 0
        if i > 17:
            currow = 3
            curcol += 1
        elif i > 11:
            currow = 2
            curcol += 1
        elif i > 5:
            currow = 1
            curcol += 1
        else:
            currow = 0
            curcol += 1

        WPLabel.grid(row=currow, column=curcol)

        # WPLabel.pack(side='left')

def delete(id):
    for wp in waypoints:
        for x in wp:
            if wp.get('id') == id:
                print(wp)
                wp.remove(x)
                tree.write('test.pln')
                # parent = tree.find('.//%s/..' % wp.tag)
                # print(parent)
                # parent.remove(wp)
    
    

canvas = tk.Canvas(window, height=600, width=800)
canvas.pack()

frame = tk.Frame(window, bg="white")
frame.place(relwidth=0.9, relheight=0.9, relx=0.05, rely=0.05)

openFile = tk.Button(window, text="Open PLN file", padx=10, pady=5,
                     fg="white", bg="#263D42", command=openPLN)
openFile.pack()

window.mainloop()
    